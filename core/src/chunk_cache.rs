use std::collections::btree_map::{BTreeMap, Range, Entry};
use std::{mem, ops};
use chunk::Chunk;
use ::Index;

pub struct ChunkCache {
    cache: BTreeMap<Index, Chunk>,
    capacity: usize,
}

impl ChunkCache {
    pub fn with_capacity(capacity: usize) -> Self {
        ChunkCache { cache: BTreeMap::new(), capacity }
    }

    pub fn cache_range(&mut self, range: ops::Range<Index>) {
        assert!(!range.is_empty());
        assert!(range.len() <= self.capacity);

        // FIXME: wrong, will not keep capacity in bound
        let mut keep = self.cache.split_off(&range.clone().next().unwrap());
        mem::swap(&mut keep, &mut self.cache);

        let seed = 1;
        let (width, height) = (11, 11);

        for index in range {
            if let Entry::Vacant(entry) = self.cache.entry(index) {
                let chunk = Chunk::generate(width, height, seed, index);
                entry.insert(chunk);
            }
        }
    }

    pub fn chunks_in_range(&self, range: ops::Range<Index>) -> Range<Index, Chunk> {
        self.cache.range(range)
    }

    pub fn len(&self) -> usize {
        self.cache.len()
    }

    pub fn is_empty(&self) -> bool {
        self.cache.is_empty()
    }

    pub fn capacity(&self) -> usize {
        self.capacity
    }
}
