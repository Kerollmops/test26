use std::ops::{Add, Sub, Mul, Div};

#[derive(Copy, Clone, Debug)]
pub struct Vector(pub f32, pub f32);

impl Vector {
    pub fn new(x: f32, y: f32) -> Vector {
        Vector(x, y)
    }

    pub fn normal(&self, other: Vector) -> Vector {
        Vector(-(other.1 - self.1), other.0 - self.0)
    }

    pub fn length(&self) -> f32 {
        (self.0 * self.0 + self.1 * self.1).sqrt()
    }

    pub fn normalize(&self) -> Vector {
        let length = self.length();
        Vector(self.0 / length, self.1 / length)
    }

    pub fn dot(&self, other: &Vector) -> f32 {
        self.0 * other.0 + self.1 * other.1
    }
}

impl Add<Vector> for Vector {
    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        Vector(self.0 + other.0, self.1 + other.1)
    }
}

impl Add<f32> for Vector {
    type Output = Vector;

    fn add(self, other: f32) -> Vector {
        Vector(self.0 + other, self.1 + other)
    }
}

impl Sub<Vector> for Vector {
    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        Vector(self.0 - other.0, self.1 - other.1)
    }
}

impl Sub<f32> for Vector {
    type Output = Vector;

    fn sub(self, other: f32) -> Vector {
        Vector(self.0 - other, self.1 - other)
    }
}

impl Mul<Vector> for Vector {
    type Output = Vector;

    fn mul(self, other: Vector) -> Vector {
        Vector(self.0 * other.0, self.1 * other.1)
    }
}

impl Mul<f32> for Vector {
    type Output = Vector;

    fn mul(self, other: f32) -> Vector {
        Vector(self.0 * other, self.1 * other)
    }
}

impl Div<Vector> for Vector {
    type Output = Vector;

    fn div(self, other: Vector) -> Vector {
        Vector(self.0 / other.0, self.1 / other.1)
    }
}

impl Div<f32> for Vector {
    type Output = Vector;

    fn div(self, other: f32) -> Vector {
        Vector(self.0 / other, self.1 / other)
    }
}
