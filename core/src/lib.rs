#![feature(exact_size_is_empty)]

mod chunk;
mod chunk_cache;

mod road;
mod vector;
mod point;
mod curve;
mod math;
mod misc;

pub use chunk::Chunk;
pub use chunk_cache::ChunkCache;

pub use road::Road;
pub use vector::Vector;
pub use point::Point;

pub use curve::Curve;
pub use curve::Bezier;
pub use curve::Spline;

pub use math::lerp;
pub use math::abs;
pub use math::fact;

pub use misc::clamp;

pub type Index = u32;
