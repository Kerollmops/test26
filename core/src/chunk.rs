use point::Point;
use self::Direc::*;
use ::Index;

fn xor_shift_64_star(mut x: u64) -> u64 {
    x ^= x >> 12; // a
    x ^= x << 25; // b
    x ^= x >> 27; // c
    x
}

#[derive(Copy, Clone, Debug)]
enum Direc {
    Left,
    Up,
    Right,
    Down,
}

#[derive(Debug)]
pub struct Chunk {
    control_points: Box<[Point]>,
}

impl Chunk {
    pub fn generate(width: usize, height: usize, seed: u32, index: Index) -> Self {
        let mut grid_raw = vec![-1; width * height];
        let mut grid_base: Vec<_> = grid_raw.as_mut_slice().chunks_mut(width).collect();
        let grid = grid_base.as_mut_slice();
        let mut points = Vec::with_capacity(width * height);

        fn compose_seq(seed: u32, dir: Direc) -> [Direc; 3] {
            fn dir_seq(seed: u32, a: Direc, b: Direc, c: Direc) -> [Direc; 3] {
                match xor_shift_64_star(u64::from(seed)) % 6 {
                    0 => [a, b, c],
                    1 => [a, c, b],
                    2 => [b, a, c],
                    3 => [b, c, a],
                    4 => [c, a, b],
                    _ => [c, b, a],
                }
            }
            match dir {
                Left =>  dir_seq(seed, Up,   Down, Left),
                Up =>    dir_seq(seed, Left, Up,   Right),
                Right => dir_seq(seed, Up,   Down, Right),
                Down =>  dir_seq(seed, Down, Left, Right),
            }
        }

        fn draw(seed: u32,
                points: &mut Vec<Point>,
                grid: &mut [&mut [i16]],
                width: usize,
                height: usize,
                x: usize,
                y: usize,
                depth: i16,
                dir: Direc)
                -> bool
        {
            // if tile not empty
            if grid[y][x] != -1 {
                return false;
            }
            grid[y][x] = depth;

            points.push(Point::new(x as f32, y as f32));

            // is end of road
            if x == width / 2 && y == height - 1 {
                return true;
            }

            for next in &compose_seq(seed, dir) {
                let (seed, x, y) = match *next {
                    Left  if x > 0 =>          (seed + 1, x - 1, y),
                    Up    if y < height - 1 => (seed + 2, x,     y + 1),
                    Right if x < width - 1 =>  (seed + 3, x + 1, y),
                    Down  if y > 0 =>          (seed + 4, x,     y - 1),
                    _ => continue,
                };

                if draw(seed, points, grid, width, height, x, y, depth + 1, *next) {
                    return true;
                }
            }

            points.pop();
            grid[y][x] = 0;
            false
        };

        points.push(Point::new((width / 2) as f32, -1.0)); // start control point
        draw(seed * index, &mut points, grid, width, height, width / 2, 0, 0, Up);
        points.push(Point::new((width / 2) as f32, height as f32)); // end control point

        for p in &mut points {
            *p = (*p - Point::new((width / 2) as f32, (height / 2) as f32)) /
                 Point::new((width - 1) as f32, (height + 1) as f32) *
                 2.0;
        }

        Chunk { control_points: points.into_boxed_slice() }
    }

    pub fn control_points(&self) -> &[Point] {
        &self.control_points
    }
}
