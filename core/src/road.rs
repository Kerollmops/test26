use std::vec::Vec;
use std::slice::Iter;
use point::Point;
use curve::Curve;

#[derive(Debug)]
pub struct Road {
    left: Vec<Point>,
    right: Vec<Point>,
}

impl Road {
    pub fn with_capacity(size: usize) -> Road {
        Road {
            left: Vec::with_capacity(size),
            right: Vec::with_capacity(size),
        }
    }

    pub fn from_curve<T: Curve>(curve: &T, road_widths: &[f32]) -> Road {
        // assert_eq!(road_widths.len(), curve.len());
        let road_width = road_widths[0]; // FIXME

        let mut result = Road {
            left: Vec::with_capacity(curve.len() + 2),
            right: Vec::with_capacity(curve.len() + 2),
        };

        let points = curve.get_points();
        let horizontal = Point::new(1.0, 0.0);

        let first = *points.first().unwrap();
        result.push(first + horizontal * road_width, first - horizontal * road_width);

        for win in points.windows(3) {
            let vec0 = win[0];
            let vec2 = win[2];
            let normal = vec0.normal(vec2).normalize();
            result.push(win[1] + normal * road_width, win[1] - normal * road_width);
        }

        let last = *points.last().unwrap();
        result.push(last + horizontal * road_width, last - horizontal * road_width);

        result
    }

    pub fn push(&mut self, left: Point, right: Point) {
        self.left.push(left);
        self.right.push(right);
    }

    pub fn get_sections(&self) -> (Iter<Point>, Iter<Point>) {
        (self.left.iter(), self.right.iter())
    }
}
