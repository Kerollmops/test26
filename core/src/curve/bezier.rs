use super::Curve;
use point::Point;
use std::vec::Vec;
use math::lerp;

#[derive(Debug)]
pub struct Bezier {
    control_points: Vec<Point>,
}

impl Bezier {
    pub fn new(control_points: &[Point]) -> Bezier {
        Bezier { control_points: control_points.to_vec() }
    }
}

impl Curve for Bezier {
/// De Casteljau Algorithm
    fn get_points(&self) -> Vec<Point> {
        fn curve(points: &[Point], t: f32) -> Point {
            let mut new = Vec::with_capacity(points.len() - 1);
            for pts_pair in points.windows(2) {
                new.push(lerp(pts_pair[0], pts_pair[1], t));
            }
            match new.len() {
                1 => new[0],
                _ => curve(&new, t)
            }
        }

        let steps = self.control_points.len() as u32 * 20;
        let step = 1.0 / steps as f32;
        let mut result = Vec::with_capacity((1.0f32 / step) as usize);
        for x in 0..steps + 1 {
            let u = x as f32 / steps as f32;
            result.push(curve(&self.control_points, u));
        }
        result
    }

    fn get_first_leg_tangent(&self) -> Point {
        self.control_points[0] - self.control_points[1]
    }

    fn get_last_leg_tangent(&self) -> Point {
        let len = self.control_points.len();
        self.control_points[len - 1] - self.control_points[len - 2]
    }

    fn len(&self) -> usize {
        self.control_points.len()
    }

    fn is_empty(&self) -> bool {
        self.control_points.is_empty()
    }
}
