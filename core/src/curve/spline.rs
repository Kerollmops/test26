use super::Curve;
use point::Point;

#[derive(Debug)]
pub struct Spline {
    a: Point,
    b: Point,
    ta: Point,
    tb: Point,
}

impl Spline {
    pub fn new(a: Point, b: Point, tangent_a: Point, tangent_b: Point) -> Spline {
        Spline {
            a: a,
            b: b,
            ta: tangent_a,
            tb: tangent_b,
        }
    }
}

impl Curve for Spline {
    /// http://www.tutorialspoint.com/computer_graphics/computer_graphics_curves.htm
    /// http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/B-spline/bspline-curve.html
    /// http://cubic.org/docs/hermite.htm
    fn get_points(&self) -> Vec<Point> {
        let step = 0.01f32;
        let n = (1.0f32 / step) as usize;
        let mut result = Vec::with_capacity(n);
        let mut s = 0.0f32;
        for _ in 0..n {
            let h1 = 2.0 * (s * s * s) - 3.0 * (s * s) + 1.0;   // calculate basis function 1
            let h2 = -2.0 * (s * s * s) + 3.0 * (s * s);        // calculate basis function 2
            let h3 = (s * s * s) - 2.0 * (s * s) + s;           // calculate basis function 3
            let h4 = (s * s * s) - (s * s);                     // calculate basis function 4
            let p = self.a * h1 +                               // multiply and sum all funtions
                    self.b * h2 +                               // together to build the interpolated
                    self.ta * h3 +                              // point along the curve.
                    self.tb * h4;
            result.push(p);
            s += step;
        }
        result
    }

    fn get_first_leg_tangent(&self) -> Point {
        self.ta
    }

    fn get_last_leg_tangent(&self) -> Point {
        self.tb
    }

    fn len(&self) -> usize {
        2
    }
}
