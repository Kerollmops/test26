mod bezier;
mod spline;

pub use self::bezier::Bezier;
pub use self::spline::Spline;

use point::Point;
use std::vec::Vec;

pub trait Curve {
    fn get_points(&self) -> Vec<Point>;
    fn get_first_leg_tangent(&self) -> Point;
    fn get_last_leg_tangent(&self) -> Point;

    fn len(&self) -> usize;
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}
