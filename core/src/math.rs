use std::ops::{ Mul, Add, Neg };
use std::cmp::PartialOrd;

pub fn lerp<T>(v0: T, v1: T, t: f32) -> T
    where T: Mul<f32, Output=T> + Add<T, Output=T> {
    v0 * t + v1 * (1.0 - t)
}

// use std::num::Zero;
// pub fn abs<T>(x: T) -> T
//     where T: Copy + PartialOrd + Zero + Neg<Output=T> {
//     if x < T::zero() { -x } else { x }
// }

pub fn abs<T>(x: T) -> T
    where T: Copy + PartialOrd + Neg<Output=T> {
    let neg = -x;
    if x < neg { neg } else { x }
}

pub fn fact(n: usize) -> f32 {
    let mut result = 1.0f32;
    for x in 1..n + 1 {
        result *= x as f32;
    }
    result
}
