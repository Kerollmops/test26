use std::cmp::PartialOrd;

pub fn clamp<'a, T: 'a + PartialOrd>(x: &'a T, min: &'a T, max: &'a T) -> &'a T {
    if x < min { min } else if x > max { max } else { x }
}
