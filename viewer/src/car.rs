use std::collections::VecDeque;
use camera::Camera;
use common::{ abs, Road, ChunkStruct, Curve, Bezier, Point, Vector };
use glium::{ DisplayBuild, Surface, Frame, Program };
use glium::vertex::VertexBufferAny;
use glium::index::IndexBufferAny;
use glium::backend::Facade;
use graphic_object::{ToGraphics, GraphicObject};

pub struct Car {
    body: GraphicObject,
    wheels: GraphicObject,
    speed: f32,
    texture: u32
}

impl Car {
    fn new(body: GraphicObject, wheel: GraphicObject, speed: f32, texture: u32) -> Car {
        Car {body: body, wheels: wheel, speed: speed, texture: texture}
    }

    fn draw(&self, target: &mut Frame, program: &Program, camera: &Camera) {
        self.body.draw(target, program, camera);
        self.wheels.draw(target, program, camera);
    }

    fn get_position(&self) -> (f32, f32) {
        (self.body.position.0, self.body.position.1)
    }

    fn set_position(&mut self, position: (f32, f32)) {
        self.body.position.0 = position.0;
        self.body.position.1 = position.1;
    }

    fn set_speed(&mut self, speed: f32) {
        self.speed = speed
    }

    fn get_speed(&self) -> f32 {
        self.speed
    }
}

