extern crate glium;

//use glium::{DisplayBuild, Surface};
//use glium::index::PrimitiveType;
//use glium::glutin;
use glium::Frame;
use glium::Program;
use glium::vertex::VertexBufferAny;
use glium::index::IndexBufferAny;
use glium::{DisplayBuild, Surface};
use glium::glutin::ElementState::*;
use glium::glutin::VirtualKeyCode::*;
use glium::DrawParameters;
//use glium::IndexBuffer;
use common::Point;
use std::vec::Vec;
use camera::Camera;
use glium::backend::Facade;

pub struct GraphicObject {
    pub vao: VertexBufferAny,
    pub position: Point,
        rotation: f32,
        scale: f32,
    pub indice_buffer: IndexBufferAny
}

pub trait ToGraphics {
    fn to_graphic_object<F>(&self, facade: &F) -> GraphicObject where F: Facade;
}

impl GraphicObject {

    pub fn raw(vao: VertexBufferAny, id_buff: IndexBufferAny) -> GraphicObject {
        GraphicObject {vao: vao,
            position: Point::new(0.0f32, 0.0),
            rotation: 0.0,
            scale: 1.0,
            indice_buffer: id_buff
        }
    }

    pub fn draw(&self, target: &mut Frame, program: &Program, camera: &Camera) {

        let uniforms = uniform!{
            camera: [camera.position.0, camera.position.1],
            camera_scale: camera.zoom,
            model_scale: self.scale,
            model_position: [self.position.0, self.position.1],
            model_rotation: self.rotation,
            camera_rotation: camera.rotation,
            camera_ratio: camera.ratio
        };
        (*target).draw(&self.vao, &self.indice_buffer, program, &uniforms, &Default::default()).unwrap();
    }

    pub fn set_position(&mut self, pos: Point) -> &mut Self {
        self.position = pos;
        self
    }

    pub fn set_rotation(&mut self, rot: f32) -> &mut Self {
        self.rotation = rot;
        self
    }

    pub fn set_scale(&mut self, scale: f32) -> &mut Self {
        self.scale = scale;
        self
    }
}
