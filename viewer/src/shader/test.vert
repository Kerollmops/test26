#version 140

in vec3 position;

out vec2 vposition;

// uniform vec2 camera;
// uniform vec2 model_position;
// uniform float camera_scale;
// uniform float model_scale;
// uniform float camera_rotation;
// uniform float model_rotation;
// uniform float camera_ratio;

vec4 rotate(float rotation, vec4 point)
{
    vec4 new;

    new.x = point.x * cos(rotation) - point.y * sin(rotation);
    new.y = point.y * cos(rotation) + point.x * sin(rotation);
    return (new);
}

void main() {
    vposition = position.xz;
    vec4 tmp;
    tmp = vec4(position.x, position.z, 0.5, 1.0);
    gl_Position = tmp;
}
