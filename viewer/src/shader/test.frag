#version 140

in vec2 vposition;

out vec4 Target0;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
    vec2 vpos = vposition;
    float coef = pow(rand(vpos), 1.0) + 0.5;
    coef += cos(vpos.y * 100.0) / 10.0;
    Target0 = mix(vec4(0.1, 0.1, 0.1, 1.0), vec4(0.0, 0.0, 0.0, 1.0), coef);
}
