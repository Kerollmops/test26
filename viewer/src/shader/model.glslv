uniform Locals {
	mat4 u_World;
	mat4 u_Model;
	mat4 u_View;
};

in vec3 v_Position;

//out vec4 f_Position;

void main() {
	vec4 Pos = u_View * u_World * u_Model * vec4(v_Position, 1.0);
//	f_Position = Pos;
	gl_Position = Pos;
}
