use std::ops;
use core::{Index, Road, Bezier, Chunk};
use nalgebra::{Matrix4, Point3, Vector3, Similarity3};
use camera::Camera;

fn triangle_striped_road(road: &Road) -> Vec<Point3<f32>> {
    let (left, right) = road.get_sections();
    let mut output = Vec::with_capacity(left.len() + right.len());

    for (l, r) in left.zip(right) {
        output.push(Point3::new(l.0, 0.0, l.1));
        output.push(Point3::new(r.0, 0.0, r.1));
    }
    output
}

pub fn chunk_vertices(chunk: &Chunk) -> Vec<Point3<f32>> {
    let control_points = chunk.control_points();
    let curve = Bezier::new(control_points);
    let road = Road::from_curve(&curve, &[0.1]);

    triangle_striped_road(&road)
}

pub fn chunk_model(index: Index) -> Matrix4<f32> {
    let translation = Vector3::new(0.0, 0.0, -0.5 + index as f32);
    let rotation = Vector3::new(0.0, 0.0, 0.0);

    Similarity3::new(translation, rotation, 0.5).to_homogeneous()
}

pub fn visible_chunks(camera: &Camera) -> ops::Range<Index> {
    let top_point = Point3::new(0.0, 1.0, 0.0);
    let bottom_point = Point3::new(0.0, -1.0, 0.0);

    // inverse of the perspective * transformation
    // to project a point from 2d screen to 3d world
    let proj = camera.projection.as_matrix() * camera.transformation();

    let inv_proj = proj.pseudo_inverse(0.0002); // ???

    // apply inverse perspective to 2d screen points and retrieve 3d world points
    let top_point = Point3::from_homogeneous(inv_proj * top_point.to_homogeneous()).unwrap();
    let bottom_point = Point3::from_homogeneous(inv_proj * bottom_point.to_homogeneous()).unwrap();

    let top_dir = top_point - camera.position;
    let bottom_dir = bottom_point - camera.position;

    let top_proj = camera.position + top_dir * (camera.position.y / top_dir.y).abs();
    let bottom_proj = camera.position + bottom_dir * (camera.position.y / bottom_dir.y).abs();

    let bottom_chunk = bottom_proj.z.floor();
    let bottom_chunk = bottom_chunk.max(0.0) as Index;

    let top_chunk = top_proj.z.ceil();
    let top_chunk = top_chunk.max(0.0) as Index;

    ops::Range {
        start: bottom_chunk,
        end: top_chunk,
    }
}
