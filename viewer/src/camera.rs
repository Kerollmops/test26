use nalgebra::{Perspective3, Point3, Matrix4, Vector3, Similarity3, Rotation3};

pub struct Camera {
    pub projection: Perspective3<f32>,
    pub position: Point3<f32>,
    pub x_rotation: f32,
    pub y_rotation: f32,
    pub z_rotation: f32,
}

impl Camera {
    pub fn new(ratio: f32) -> Camera {
        Camera {
            projection: Perspective3::new(ratio, 90., 0.1, 1000.),
            position: Point3::origin(),
            x_rotation: 0.0,
            y_rotation: 0.0,
            z_rotation: 0.0,
        }
    }

    pub fn update_ratio(&mut self, ratio: f32) {
        self.projection = Perspective3::new(ratio, 90., 0.1, 1000.)
    }

    pub fn transformation(&self) -> Matrix4<f32> {
        let trans = Similarity3::new(-self.position.coords, Vector3::new(0.0, 0.0, 0.0), 1.0).to_homogeneous();

        let rot_x = Rotation3::new(Vector3::new(-self.x_rotation, 0.0, 0.0)).to_homogeneous();
        let rot_y = Rotation3::new(Vector3::new(0.0, -self.y_rotation, 0.0)).to_homogeneous();
        let rot_z = Rotation3::new(Vector3::new(0.0, 0.0, -self.z_rotation)).to_homogeneous();

        rot_z * rot_x * rot_y * trans
    }
}
