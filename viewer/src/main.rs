#![feature(inclusive_range_syntax)]

#[macro_use] extern crate gfx;
extern crate glutin;
extern crate gfx_window_glutin;
extern crate nalgebra;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate core;

mod camera;
mod chunk;

use std::f32;
use glutin::ElementState::*;
use glutin::{Event, WindowEvent, VirtualKeyCode, KeyboardInput};
use glutin::GlContext;
use gfx::format::{Rgba8, DepthStencil};
use gfx::{Device};
use nalgebra::Point3;
use nalgebra::Real;
use camera::Camera;

pub type ColorFormat = Rgba8;
pub type DepthFormat = DepthStencil;

fn main() {
    let _ = env_logger::init();

    let (width, height) = (1280, 720);
    let win_builder = glutin::WindowBuilder::new()
        .with_title("Test26")
        .with_dimensions(width, height);
    let ctx_builder = glutin::ContextBuilder::new();
    let mut events_loop = glutin::EventsLoop::new();

    let (window, mut device, mut factory, color, depth) =
        gfx_window_glutin::init::<ColorFormat, DepthFormat>(win_builder, ctx_builder, &events_loop);

    let mut encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();

    let cache_size = 30;
    // let chunk_manager = ChunkManager::new(&mut factory, cache_size);

    let mut camera = Camera::new(width as f32 / height as f32);

    camera.position = Point3::new(0.0, 4.0, 0.0);
    camera.x_rotation = -f32::pi() / 2.0;
    camera.y_rotation = f32::pi();

    let mut running = true;
    while running {
        events_loop.poll_events(|event| {
            match event {
                Event::WindowEvent{ event: WindowEvent::Closed, .. } => running = false,
                Event::WindowEvent{
                    event: WindowEvent::KeyboardInput {
                        input: KeyboardInput {
                            state: Released,
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                    .. },
                .. } => running = false,
                _ => (),
            }
        });

        camera.position.z += 0.001;

        encoder.clear(&color, [0.0, 0.0, 0.0, 1.]);
        encoder.clear_depth(&depth, 1.);

        // chunk_manager.draw_chunks(&mut factory, &mut encoder, &color, &depth, &camera);
        unimplemented!("draw chunks");

        encoder.flush(&mut device);
        window.swap_buffers().unwrap();
        device.cleanup();
    }

}

