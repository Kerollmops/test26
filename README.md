# test26

First Header | Second Header
------------ | -------------
Content from cell 1 | Content from cell 2
Content in the first column | Content in the second column

## Map generation

All random generations are based on a seed created at launch.

### Chunks generation

Chunks are split in 5x5 tiles which represents the raw route. The road's algorithm starts at the bottom of the chunk (2;0), and recursively move (randomly) to a next empty tile, backtracking when blocked, until it founds the exit (2;4). This route is then smoothed to a curve and "thickened", creating a road edges.

## Car infos

The base curve is used too compute the distance  the car traveled.